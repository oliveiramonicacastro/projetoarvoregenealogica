import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapaComponent } from './mapa/mapa.component';
import { FormularioComponent } from './formulario/formulario.component';
import { PrincipalComponent } from './principal/principal.component';

@NgModule({
  declarations: [			
    AppComponent,
      MapaComponent,
      FormularioComponent,
      PrincipalComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
