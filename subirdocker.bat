docker network create jenkins
docker volume create jenkins-docker-certs
docker volume create jenkins-data

docker rm -f Jenkins jenkins-docker

rem docker run --rm -d -u root --name Jenkins --network jenkins --group-add 0 -v "/usr/bin/docker:/usr/bin/docker" -v "/var/run/docker.sock:/var/run/docker.sock" -v jenkins_home:/var/jenkins_home -p 8080:8080 -p 50000:50000  jenkins/jenkins:lts

@REM docker container run --name jenkins-docker^
@REM  --restart unless-stopped^
@REM  --detach^
@REM  --privileged --network jenkins^
@REM  --network-alias docker^
@REM  --env DOCKER_TLS_CERTDIR=/certs^
@REM  --volume jenkins-docker-certs:/certs/client^
@REM  --volume jenkins-data:/var/jenkins_home^
@REM  --publish 2376:2376^
@REM  docker:dind

@REM  --env DOCKER_HOST=tcp://docker:2376^
@REM  --env DOCKER_CERT_PATH=/certs/client^
@REM  --env DOCKER_TLS_VERIFY=1^
docker container run --name Jenkins^
 -u root^
 --group-add 0^
 --restart unless-stopped^
 --detach^
 --network jenkins^
 --volume jenkins-data:/var/jenkins_home^
 --volume jenkins-docker-certs:/certs/client:ro^
 --volume "/usr/bin/docker:/usr/bin/docker"^
 --volume "/var/run/docker.sock:/var/run/docker.sock"^
 --volume "/usr/share/zoneinfo/Etc/GMT+3:/etc/localtime"^
 --publish 8080:8080^
 --publish 50000:50000^
 jenkinsci/blueocean

docker exec -it  Jenkins  sh -c "alias ll='ls -ltr --color'"
